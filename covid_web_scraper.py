import requests
import json
import re


def make_web_request():

    cookies = {
        '_ga': 'GA1.2.1334181692.1590707758',
        '_gid': 'GA1.2.659130322.1590707758',
        'tableau_locale': 'en',
        'prism_610260786': '9dd63476-82fa-408e-ba90-7c60af683a17',
        '_fbp': 'fb.1.1590707758673.926589318',
        '_hjid': '3e2040f9-f57c-4b85-b8c0-ca914e5cfdf4',
        'workgroup_session_id': '',
    }

    headers = {
        'Connection': 'keep-alive',
        'Pragma': 'no-cache',
        'Cache-Control': 'no-cache',
        'Accept': 'text/javascript',
        'X-Tsi-Active-Tab': 'COVID-19',
        'Sec-Fetch-Dest': 'empty',
        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.149 Safari/537.36',
        'Content-Type': 'application/x-www-form-urlencoded',
        'Origin': 'https://tableau.alleghenycounty.us',
        'Sec-Fetch-Site': 'same-origin',
        'Sec-Fetch-Mode': 'cors',
        'Referer': 'https://tableau.alleghenycounty.us/t/PublicSite/views/COVID-19AlleghenyCounty/COVID-19?%3AisGuestRedirectFromVizportal=y&%3Aembed=y',
        'Accept-Language': 'en-US,en;q=0.9,la;q=0.8',
    }

    data = {
        'worksheetPortSize': '{"w":850,"h":1200}',
        'dashboardPortSize': '{"w":850,"h":1200}',
        'clientDimension': '{"w":788,"h":824}',
        'renderMapsClientSide': 'true',
        'isBrowserRendering': 'true',
        'browserRenderingThreshold': '100',
        'formatDataValueLocally': 'false',
        'clientNum': '',
        'navType': 'Reload',
        'navSrc': 'Top',
        'devicePixelRatio': '1',
        'clientRenderPixelLimit': '25000000',
        'sheet_id': 'COVID-19',
        'showParams': '{"checkpoint":false,"refresh":false,"refreshUnmodified":false}',
        'stickySessionKey': '{"featureFlags":"{}","isAuthoring":false,"isOfflineMode":false,"lastUpdatedAt":1590764045422,"viewId":12862,"workbookId":2395}',
        'filterTileSize': '200',
        'locale': 'en_US',
        'language': 'en',
        'verboseMode': 'false',
        ':session_feature_flags': '{}',
        'keychain_version': '1'
    }

    print("Making web request...")

    return requests.post(
        'https://tableau.alleghenycounty.us/vizql/t/PublicSite/w/COVID-19AlleghenyCounty/v/COVID-19/bootstrapSession/sessions/B80280F0C81941A885BB44EFEECACE71-1:1',
        headers=headers, cookies=cookies, data=data)

def clean_web_response(response_text):
    """
    For some bizarre reason, this endpoint returns data in the format:

    1875293;{<valid JSON>}174069;{<more valid JSON>}

    Where the 1875293 and 174069 numbers appear to increment every so often?

    We need to remove those two strings before we can decode the json, and then decode each of the json halves
    separately. So far it looks like the relevant data is in the first json object, but it's possible you'll also have
    to use the second json object in the future.
    """

    print("Cleaning response...")
    first_string_removed = response_text.split(";", 1)[1]
    first_and_second_string_removed = re.split("\d{4,10};{", first_string_removed)
    first_json_object = json.loads(first_and_second_string_removed[0])
    second_json_object = json.loads("{" + first_and_second_string_removed[1])

    return first_json_object, second_json_object

def print_relevant_data(first_json_object, second_json_object):
    total_cases = first_json_object["worldUpdate"]["applicationPresModel"]["workbookPresModel"]["dashboardPresModel"]["zones"]["15"]["presModelHolder"]["visual"]["lastComputedCalculation"]
    print("Total cases: {}".format(total_cases))

    total_tests = first_json_object["worldUpdate"]["applicationPresModel"]["workbookPresModel"]["dashboardPresModel"]["zones"]["14"]["presModelHolder"]["visual"]["lastComputedCalculation"]
    print("Total tests: {}".format(total_tests))

    total_deaths = first_json_object["worldUpdate"]["applicationPresModel"]["workbookPresModel"]["dashboardPresModel"]["zones"]["16"]["presModelHolder"]["visual"]["lastComputedCalculation"]
    print("Total deaths: {}".format(total_deaths))

    # TODO parse any other metrics


def main():
    print("Starting...")
    response = make_web_request()
    print("Received response with status code: {}".format(response.status_code))
    first_json_object, second_json_object = clean_web_response(response.text)
    print_relevant_data(first_json_object, second_json_object)
    print("Done!")


if __name__ == '__main__':
    main()